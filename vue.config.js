module.exports = {
  publicPath: '/',
  outputDir: 'dist',
  css: {
    loaderOptions: {
      sass: {
        implementation: require('sass'),
      },
    },
  },
};
